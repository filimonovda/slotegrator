<?php

namespace Database\Seeders;

use App\Models\Prize;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create(['email' => 'test@local']);
        User::factory()->count(10)->create();
        Prize::factory()->count(30)->isMoney()->create();
        Prize::factory()->count(30)->isTrophy()->create();
        Prize::factory()->count(30)->isPoint()->create();
    }
}
