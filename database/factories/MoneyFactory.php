<?php

namespace Database\Factories;

use App\Models\Money;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MoneyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Money::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::query()->inRandomOrder()->first()->id,
            'amount' => $this->faker->numberBetween(0, 1000),
            'transaction_id' => $this->faker->numberBetween(0, 999)
        ];
    }
}
