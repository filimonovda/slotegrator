<?php

namespace Database\Factories;

use App\Enums\TrophyType;
use App\Models\Trophy;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrophyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Trophy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'type' => TrophyType::getRandomValue()
        ];
    }
}
