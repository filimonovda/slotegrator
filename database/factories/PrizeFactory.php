<?php

namespace Database\Factories;

use App\Enums\PrizeType;
use App\Models\Money;
use App\Models\Point;
use App\Models\Prize;
use App\Models\Trophy;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrizeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Prize::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
        ];
    }

    public function isMoney(): PrizeFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'entity_type' => PrizeType::MONEY,
                'entity_id' => Money::factory()->create(['user_id' => $attributes['user_id']])
            ];
        });
    }

    public function isPoint(): PrizeFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'entity_type' => PrizeType::POINT,
                'entity_id' => Point::factory()->create(['user_id' => $attributes['user_id']])
            ];
        });
    }


    public function isTrophy(): PrizeFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'entity_type' => PrizeType::TROPHY,
                'entity_id' => Trophy::factory()->create(['user_id' => $attributes['user_id']])
            ];
        });
    }
}
