<?php

use App\Enums\PrizeType;
use App\Enums\TrophyType;

return [
    'prefix' => 'Congratulations! You prize ',
    'description' => [
        PrizeType::POINT => '{1} :amount point|[2,*] :amount points',
        PrizeType::MONEY => '{1} :amount coin|[2,*] :amount coins',
        PrizeType::TROPHY => [
            TrophyType::CANDY => 'tasty candy',
            TrophyType::BICYCLE => 'cool bicycle',
            TrophyType::BIRDHOUSE => 'cozy birdhouse',
            TrophyType::APPLE => 'juicy apple',
        ]
    ]
];
