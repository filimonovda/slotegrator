<?php

use App\Enums\TrophyType;

return [
    'limit' => [
        'money' => 1000,
        'trophy' => [
            TrophyType::APPLE => 2,
            TrophyType::BICYCLE => 2,
            TrophyType::BIRDHOUSE => 1,
            TrophyType::CANDY => 2
        ],
        'point' => 100
    ],
    'conversion_ratio_points_to_money' => 5
];
