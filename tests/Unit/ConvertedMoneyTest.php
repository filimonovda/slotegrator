<?php

namespace Tests\Unit;

use App\Models\Point;
use App\Models\Prize;
use App\Models\User;
use App\Services\Core;
use Tests\TestCase;

class ConvertedMoneyTest extends TestCase
{
    private Core $core;

    public function setUp(): void
    {
        parent::setUp();
        $this->core = $this->app->make(Core::class);
    }

    public function testNormalize()
    {
        $user = User::factory()->create();
        $point = Prize::factory()->isPoint()->create(['user_id' => $user->id]);
        $money = Prize::factory()->isMoney()->create(['user_id' => $user->id]);
        $amountMoney = $money->entity->amount;
        $this->core->converterMoneyToPoint($money->id);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'amount_points' => $amountMoney * config('system.conversion_ratio_points_to_money') + $point->entity->amount
        ]);
        $this->assertSoftDeleted('money', ['id' => $money->entity->id]);
    }

    public function testSummary()
    {
        $user = User::factory()->create();
        Prize::factory(10)->isPoint()->create(['user_id' => $user->id]);
        $money = Prize::factory()->isMoney()->create(['user_id' => $user->id]);
        $this->core->converterMoneyToPoint($money->id);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'amount_points' => Point::query()->where(['user_id' => $user->id])->sum('amount')
        ]);
    }
}
