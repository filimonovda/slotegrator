<?php

namespace App\Http\ResponseModels;

class PrizeList
{
    public string $date;
    public string $description;
}
