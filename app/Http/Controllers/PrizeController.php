<?php

namespace App\Http\Controllers;

use App\Services\Core;
use Illuminate\Support\Facades\Auth;

class PrizeController extends Controller
{
    private Core $core;

    public function __construct(Core $core)
    {
        $this->core = $core;
    }

    public function index()
    {
        return view('prize', ['data' => $this->core->getListPrizes(Auth::id())]);
    }

    public function create()
    {
        return view('dashboard', ['prize' => $this->core->getPrize(Auth::id())]);
    }
}
