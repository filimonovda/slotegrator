<?php

namespace App\Providers;

use App\Enums\PrizeType;
use App\Models\Money;
use App\Models\Point;
use App\Models\Trophy;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            PrizeType::MONEY => Money::class,
            PrizeType::POINT => Point::class,
            PrizeType::TROPHY => Trophy::class
        ]);
    }
}
