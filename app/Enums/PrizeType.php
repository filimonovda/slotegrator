<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PrizeType extends Enum
{
    const POINT = 'point';
    const TROPHY = 'trophy';
    const MONEY = 'money';
}
