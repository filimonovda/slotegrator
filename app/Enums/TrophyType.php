<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TrophyType extends Enum
{
    const BICYCLE = 'bicycle';
    const APPLE = 'apple';
    const BIRDHOUSE = 'birdhouse';
    const CANDY = 'candy';
}
