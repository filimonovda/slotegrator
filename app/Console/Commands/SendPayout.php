<?php

namespace App\Console\Commands;

use App\Models\Money;
use App\Services\Bank;
use Illuminate\Console\Command;

class SendPayout extends Command
{
    protected $signature = 'send:payout {count=10}';
    protected $description = 'Произвести выплаты выигрышей';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Bank $bank)
    {
        Money::query()
            ->where('transaction_id', '=', 0)
            ->limit($this->argument('count'))
            ->get()
            ->each(function (Money $money) use ($bank) {
                $money->update(['transaction_id' => $bank->getTransaction()]);
            });
    }
}
