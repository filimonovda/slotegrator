<?php

namespace App\Observers;

use App\Models\Point;
use App\Models\User;

class PointObserver
{
    public function created(Point $point)
    {
        User::query()
            ->where('id', '=', $point->user_id)
            ->increment('amount_points', $point->amount);
    }
}
