<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Prize
 * @package App\Models
 *
 * @property Carbon $created_at
 * @property string $entity_type
 * @property integer $user_id
 * @property int $entity_id
 * @property IPrize $entity
 */
class Prize extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['user_id', 'entity_type', 'entity_id'];

    public function entity(): MorphTo
    {
        return $this->morphTo();
    }
}
