<?php

namespace App\Models;

use App\Enums\PrizeType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Trophy
 * @package App\Models
 *
 * @property integer $id
 * @property string $type
 * @property integer $user_id
 */
class Trophy extends Model implements IPrize
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['user_id', 'type'];

    public function prize(): MorphOne
    {
        return $this->morphOne(Prize::class, 'entity');
    }

    public function getDescription(): string
    {
        return trans('prize.description.' . PrizeType::TROPHY . '.' . $this->type);
    }
}
