<?php

namespace App\Models;

interface IPrize
{
    public function getDescription(): string;
}
