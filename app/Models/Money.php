<?php

namespace App\Models;

use App\Enums\PrizeType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Money
 * @package App\Models
 *
 * @property integer $amount
 * @property integer $user_id
 */
class Money extends Model implements IPrize
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['user_id', 'amount', 'transaction_id'];

    public function prize(): MorphOne
    {
        return $this->morphOne(Prize::class, 'entity');
    }

    public function getDescription(): string
    {
        return trans_choice('prize.description.' . PrizeType::MONEY, $this->amount, ['amount' => $this->amount]);
    }
}
