<?php

namespace App\Services;

use App\Services\PrizeType\IPrizeTypeService;

class PrizeService
{
    private IPrizeTypeService $prizeService;

    public function __construct(IPrizeTypeService $prizeService)
    {
        $this->prizeService = $prizeService;
    }

    public function isAvailable(): bool
    {
        return $this->prizeService->isAvailable();
    }

    public function processing(int $userId): string
    {
        $this->prizeService->processing($userId);
        return $this->prizeService->getDescription();
    }
}
