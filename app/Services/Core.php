<?php

namespace App\Services;

use App\Enums\PrizeType;
use App\Http\ResponseModels\PrizeList;
use App\Models\Money;
use App\Models\Point;
use App\Models\Prize;
use Illuminate\Support\Arr;

class Core
{
    private PrizeBuilderServices $prizeBuilderServices;

    public function __construct(PrizeBuilderServices $prizeBuilderServices)
    {
        $this->prizeBuilderServices = $prizeBuilderServices;
    }

    public function getListPrizes(int $userId): array
    {
        return Prize::with('entity')
            ->where('user_id', '=', $userId)
            ->get()
            ->map(function (Prize $item) {
                $modelResponse = new PrizeList();
                $modelResponse->date = $item->created_at->toDateTimeString();
                $modelResponse->description = $item->entity->getDescription();
                return $modelResponse;
            })
            ->toArray();
    }

    public function getPrize(int $userId): string
    {
        foreach (Arr::shuffle(PrizeType::getKeys()) as $typeKey) {
            $instancePrize = $this->prizeBuilderServices->get(PrizeType::getValue($typeKey));
            if ($instancePrize->isAvailable()) {
                return $instancePrize->processing($userId);
            }
        }

        return '';
    }

    public function converterMoneyToPoint(int $idPrize)
    {
        $prize = Prize::with('entity')->find($idPrize);
        $newPrize = Point::query()
            ->create([
                'user_id' => $prize->user_id,
                'amount' => $prize->entity->amount * config('system.conversion_ratio_points_to_money')
            ]);
        Money::query()->find($prize->entity_id)->delete();
        $prize->update([
            'entity_type' => PrizeType::POINT,
            'entity_id' => $newPrize->id
        ]);
    }
}
