<?php

namespace App\Services\PrizeType;

use App\Enums\PrizeType;
use App\Models\Prize;
use App\Models\Trophy;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

final class TrophyPrizeType extends PrizeTypeService
{
    private ?array $actualLimit = null;

    protected function actualLimit(): void
    {
        if (!Cache::has('actual_trophy_limit')) {
            $this->updateCache();
        }

        $actualLimit = Cache::get('actual_trophy_limit');
        $this->actualLimit = $actualLimit;
        $this->isAvailable = count($actualLimit);
    }

    public function processing(int $userId): void
    {
        $this->object = Trophy::query()
            ->create([
                'user_id' => $userId,
                'type' => Arr::random($this->actualLimit)
            ]);
        Prize::query()
            ->create([
                'user_id' => $userId,
                'entity_type' => PrizeType::TROPHY,
                'entity_id' => $this->object->id
            ]);
        $this->updateCache();
    }

    private function updateCache()
    {
        $result = config('system.limit.trophy');
        Trophy::query()
            ->select('type', DB::raw('count(*) as count'))
            ->groupBy('type')
            ->get()
            ->map(function ($item) use (&$result) {
                $result[$item->type] = config('system.limit.trophy.' . $item->type) - $item->count;
            })
            ->all();

        Cache::put(
            'actual_trophy_limit',
            array_keys(array_filter($result))
        );
    }
}
