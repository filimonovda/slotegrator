<?php

namespace App\Services\PrizeType;

use App\Models\IPrize;

abstract class PrizeTypeService implements IPrizeTypeService
{
    protected ?bool $isAvailable = null;
    protected IPrize $object;

    public function isAvailable(): bool
    {
        if (is_null($this->isAvailable)) {
            $this->actualLimit();
        }

        return $this->isAvailable;
    }

    abstract protected function actualLimit(): void;

    public function getDescription(): string
    {
        return $this->object->getDescription();
    }
}
