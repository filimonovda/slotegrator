<?php

namespace App\Services\PrizeType;

use App\Enums\PrizeType;
use App\Models\Money;
use App\Models\Prize;
use Illuminate\Support\Facades\Cache;

final class MoneyPrizeType extends PrizeTypeService
{
    private ?int $actualLimit = null;

    protected function actualLimit(): void
    {
        if (!Cache::has('actual_money_limit')) {
            $this->updateCache();
        }
        $actualLimit = Cache::get('actual_money_limit');
        $this->actualLimit = $actualLimit;
        $this->isAvailable = $actualLimit > 0;
    }

    public function processing(int $userId): void
    {
        $this->object = Money::query()
            ->create([
                'user_id' => $userId,
                'amount' => rand(1, $this->actualLimit),
                'paid' => false
            ]);
        Prize::query()
            ->create([
                'user_id' => $userId,
                'entity_type' => PrizeType::MONEY,
                'entity_id' => $this->object->id
            ]);
        $this->updateCache();
    }

    private function updateCache()
    {
        Cache::put(
            'actual_money_limit',
            config('system.limit.money') - Money::query()->sum('amount')
        );
    }
}
