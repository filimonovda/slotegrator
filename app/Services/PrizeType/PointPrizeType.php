<?php

namespace App\Services\PrizeType;

use App\Enums\PrizeType;
use App\Models\Point;
use App\Models\Prize;

final class PointPrizeType extends PrizeTypeService
{
    public function isAvailable(): bool
    {
        return true;
    }

    public function processing(int $userId): void
    {
        $this->object = Point::query()
            ->create([
                'user_id' => $userId,
                'amount' => rand(1, config('system.limit.point'))
            ]);
        Prize::query()
            ->create([
                'user_id' => $userId,
                'entity_type' => PrizeType::POINT,
                'entity_id' => $this->object->id
            ]);
    }

    protected function actualLimit(): void
    {
        //not implement in the type.
    }
}
