<?php

namespace App\Services\PrizeType;

interface IPrizeTypeService
{
    public function isAvailable(): bool;

    public function processing(int $userId): void;

    public function getDescription(): string;
}
