<?php

namespace App\Services;

use App\Enums\PrizeType;
use App\Services\PrizeType\MoneyPrizeType;
use App\Services\PrizeType\PointPrizeType;
use App\Services\PrizeType\TrophyPrizeType;

class PrizeBuilderServices
{
    private array $mapPrizes = [
        PrizeType::TROPHY => TrophyPrizeType::class,
        PrizeType::MONEY => MoneyPrizeType::class,
        PrizeType::POINT => PointPrizeType::class
    ];

    public function get(string $type): PrizeService
    {
        return new PrizeService(new $this->mapPrizes[$type]);
    }
}
